# Freachly Challenge

A Node.js REST API built on Express and routing-controllers utilizing Mongoose.

## Prerequisites

- docker and docker-compose to start:
   - mongodb 
   - mongo express client
- Node.js version >= 12

## Setup

Install the dependencies

    npm install

## Run

Start the development environment by initially starting up mongodb and mongo express client containers by running

    docker-compose up -d

See mongo express: `http://localhost:8081`

Start the application in development mode by running

    npm run develop

This will run the grunt script, which will watch for changes, run tsc and restart the application.

**OR** 

by building and starting the application

    npm run build
    npm start

The service will be available at: `http://localhost:3000/api/v1`

## Test

Run the lint tests by running

    npm run lint

Run the integration tests by running

    npm test
