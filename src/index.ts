import "reflect-metadata";

import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import Container from "typedi";

import { useExpressServer } from "routing-controllers";
import { Config } from "./services/config";
import { PersitenceClient } from "./services/persistence.client";
import { Logger } from "./services/logger";

export class App {

    public run(): void {

        const app = express();
        const config = new Config().init();
        const logger = new Logger();

        Container.set(Logger, logger);
        Container.set(Config, config);
        Container.set(PersitenceClient, new PersitenceClient().connect());

        app.use(bodyParser.json());
        app.use(cors());

        useExpressServer(app, {
            routePrefix: "/api/v1",
            controllers: [__dirname + "/controllers/*.js"],
            middlewares: [__dirname + "/middlewares/*.js"],
            defaultErrorHandler: false,
        });

        app.listen(3000, "0.0.0.0", () => logger.info("Server listening on port 3000"));
    }

}

new App().run();