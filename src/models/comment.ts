import { Document, model, Schema } from "mongoose";

export interface IComment extends Document {
    id: string;
    hashtags: string[];
    mentions: string[];
    text: string;
    timestamp: number;
    userId: string;
}

const CommentSchema: Schema = new Schema({
    id: { type: String, required: true, unique: true },
    hashtags: { type: Array, required: false },
    mentions: { type: Array, required: false },
    text: { type: String, required: true },
    timestamp: { type: Number, default: Date.now },
    userId: { type: String, required: true, index: true },
});

CommentSchema
    .index({ id: 1 }, { unique: true })
    .index({ userId: 1 });

export function getTop10MentionsAggregationStages(): unknown[] {
    return getTop10AggregationStages("mentions");
}

export function getTop10HashTagsAggregationStages(): unknown[] {
    return getTop10AggregationStages("hashtags");
}

export default model<IComment>("Comment", CommentSchema);

function getTop10AggregationStages(arrayKey: string): unknown[] {
    return [
        { $unwind: "$" + arrayKey },
        { $group: { _id: "$" + arrayKey, n: { $sum: 1 } } },
        { $sort: { n: -1 } },
        { $limit: 10 }
    ];
}