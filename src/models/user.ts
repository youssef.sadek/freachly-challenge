
import { Contact } from "./contact";

import mongoose, { Schema, Document } from "mongoose";

export interface IUser extends Document {
    contact: Contact;
    id: string;
    profilePictureUrl: string;
    updatedAt: number;
    username: string;
}

const UserSchema: Schema = new Schema({
    contact: { type: Object, required: false },
    id: { type: String, required: true },
    profilePictureUrl: { type: String, required: false },
    updatedAt: { type: Number, default: Date.now },
    username: { type: String, required: true },
});

UserSchema.index({ id: 1 }, { unique: true });

export default mongoose.model<IUser>("User", UserSchema);