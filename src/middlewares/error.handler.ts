import { Request, Response } from "express";
import { Middleware, ExpressErrorMiddlewareInterface, HttpError } from "routing-controllers";
import Container from "typedi";
import { Logger } from "../services/logger";

@Middleware({ type: "after" })
export class ErrorHandler implements ExpressErrorMiddlewareInterface {
    private logger: Logger = Container.get(Logger)

    public error(error: Error, request: Request, response: Response, next: (err?: Error) => unknown): void {

        if (error instanceof HttpError) {
            // process dedicated http error.
            response.status(error.httpCode).send({
                message: error.message,
            });
        } else {
            // handle any unexpected error as bad request.
            response.status(400).send({
                message: "Ups... that came unexpected",
            });
        }

        this.logger.error(error.message);
        next(error);
    }

}
