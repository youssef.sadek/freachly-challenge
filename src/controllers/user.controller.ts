import { Body, Delete, Get, JsonController, Param, Post, Put } from "routing-controllers";
import User, { IUser } from "../models/user";

@JsonController("/user")
export class UserController {

    @Post("/")
    public async create(@Body() user: IUser): Promise<IUser> {
        return await (await User.create<IUser>(user)).toObject();
    }

    @Put("/:userId")
    public async update(@Param("userId") userId: string, @Body() user: IUser): Promise<IUser> {
        await User.updateOne({ id: userId }, user);
        return user;
    }

    @Get("/:userId")
    public async read(@Param("userId") userId: string): Promise<IUser> {
        const userQuery = await User.findOne({ id: userId });
        return userQuery?.toObject();
    }

    @Delete("/:userId")
    public async delete(@Param("userId") userId: string): Promise<boolean> {
        await User.deleteOne({ id: userId });
        return true;
    }

}