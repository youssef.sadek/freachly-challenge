import { Body, Delete, Get, JsonController, Param, Post, Put } from "routing-controllers";
import Comment,
{
    getTop10HashTagsAggregationStages,
    getTop10MentionsAggregationStages,
    IComment
} from "../models/comment";

@JsonController("/comment")
export class CommentController {

    @Post("/")
    public async create(@Body() comment: IComment): Promise<IComment> {
        const commentDocument = await Comment.create(comment);
        return commentDocument.toObject();
    }

    @Put("/:commentId")
    public async update(@Body() comment: IComment, @Param("commentId") commentId: string): Promise<IComment> {
        await Comment.updateOne({ id: commentId }, comment);
        return comment;
    }

    @Get("/user/:userId")
    public async readCommentsByUser(@Param("userId") userId: string): Promise<IComment[]> {
        const queryDoc = await Comment.find({ userId: userId });
        return queryDoc.map(d => d.toObject());
    }


    @Get("/:commentId")
    public async read(@Param("commentId") commentId: string): Promise<IComment> {
        const queryDoc = await Comment.findOne({ id: commentId });
        return queryDoc?.toObject();
    }

    @Delete("/:commentId")
    public async delete(@Param("commentId") commentId: string): Promise<boolean> {
        await Comment.deleteOne({ id: commentId });
        return true;
    }

    @Get("/aggregate/top/mentions")
    public async aggregateTopMentions(): Promise<{ _id: string, n: number }[]> {
        return await Comment.aggregate(getTop10MentionsAggregationStages());
    }

    @Get("/aggregate/top/hashtags")
    public async aggregateTopHashTags(): Promise<{ _id: string, n: number }[]> {
        return await Comment.aggregate(getTop10HashTagsAggregationStages());
    }

}