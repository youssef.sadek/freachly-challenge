import mongoose from "mongoose";
import Container from "typedi";
import { Config } from "./config";
import { Logger } from "./logger";

export class PersitenceClient {
    private config = Container.get(Config);
    private logger = Container.get(Logger);

    public async connect(): Promise<PersitenceClient> {
        await mongoose.connect(this.config.mongoConnectionString, {
            authSource: "admin",
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            auth: {
                user: this.config.mongoUsername,
                password: this.config.mongoPassword,
            }
        });
        this.logger.info("Connection to database successful");

        mongoose.connection.on("disconnected", () => {
            this.logger.info("Disconnected, reconnecting...");
            this.connect();
        }).on("error", (error: mongoose.Error) => this.logger.error(error.message));

        return this;
    }

}