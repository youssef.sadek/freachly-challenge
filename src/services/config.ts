export class Config {
    public mongoUsername = "root";
    public mongoPassword = "example"
    public mongoEndpoint = "localhost:27017";
    public dbName = "freachly";
    public mongoConnectionString: string;

    private config: Config | undefined;

    constructor() {
        this.mongoConnectionString = `mongodb://${this.mongoEndpoint}/${this.dbName}`
    }

    public init(): Config {
        if (this.config === undefined) {
            this.config = new Config();
        }
        return this.config;
    }
}