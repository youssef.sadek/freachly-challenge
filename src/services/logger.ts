import winston from "winston";

export class Logger {
    private logger: winston.Logger = winston.createLogger({
        level: "debug",
        format: winston.format.prettyPrint(),
        transports: [
            new winston.transports.Console(),
        ],
    });

    public info(message: string): void {
        this.logger.info(message);
    }

    public debug(message: string): void {
        this.logger.debug(message);
    }

    public error(message: string): void {
        this.logger.error(message);
    }
}