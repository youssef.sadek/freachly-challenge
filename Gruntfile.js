module.exports = function (grunt) {
  grunt.initConfig({
    clean: {
      build: ["dist"]
    },
    ts: {
      default: {
        tsconfig: true
      },
      options: {
        fast: "never"
      }
    },
    nodemon: {
      dev: {
        script: "dist/src/index.js"
      }
    },
    watch: {
      scripts: {
        files: ["Gruntfile.js", "src/**/*.ts"],
        tasks: ["ts", "eslint"],
        options: {
          interrupt: true,
        },
      },
    },
    eslint: {
      target: ["src/**/*.ts"]
    },
    concurrent: {
      target: {
        tasks: ["nodemon", "watch"],
        options: {
          logConcurrentOutput: true
        }
      }
    },
    mochaTest: {
      test: {
        options: {
          quiet: false,
          clearRequireCache: false,
          noFail: false,
          timeout: 50000
        },
        src: [
          // int test
          "dist/test/controllers/health.controller.spec.js",
          "dist/test/controllers/user.controller.spec.js",
        ]
      }
    },
  });
  grunt.loadNpmTasks("grunt-ts");
  grunt.loadNpmTasks("grunt-eslint");
  grunt.loadNpmTasks("grunt-contrib-nodemon");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-concurrent");
  grunt.loadNpmTasks("grunt-mocha-test");

  grunt.registerTask("build", ["clean", "ts"]);
  grunt.registerTask("default", ["clean", "ts", "eslint", "concurrent"]);
};
