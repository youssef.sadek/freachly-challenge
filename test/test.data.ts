import { Contact } from "../src/models/contact";
import * as uuid from "uuid";

export const mockUser = {
    contact: new Contact("", "", ""),
    id: uuid.v4(),
    profilePictureUrl: "http://google.com",
    username: "wanda",
    updatedAt: new Date().getTime(),
};

export const mockComment = {
    userId: mockUser.id,
    id: uuid.v4(),
    text: "What comment!",
    hashtags: ["#s0mmer"],
    mentions: ["wanda"],
};