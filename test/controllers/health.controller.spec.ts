import { expect } from "chai";
import { describe, it } from "mocha";
import * as supertest from "supertest";
import { TestContext } from "../test.context";

describe("Health controller test", () => {

    const request = supertest.agent(TestContext.apiEndpoint);

    it("should request the ressource successfully", (done) => {
        request.get("/health").end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });

});