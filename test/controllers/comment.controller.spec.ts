import { expect } from "chai";
import { describe, it } from "mocha";
import * as supertest from "supertest";
import { TestContext } from "../test.context";
import { mockComment } from "../test.data";

describe("Comment controller test", () => {

    const request = supertest.agent(TestContext.apiEndpoint);

    it("Should create a comment successfully", (done) => {
        request.post("/comment").send(mockComment).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });

    it("Should fail to create identical comment", (done) => {
        request.post("/comment").send(mockComment).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(400);
            done();
        });
    });

    it("Should reject the invalid comment creation request", (done) => {
        const invalidComment = {};
        request.post("/comment").send(invalidComment).end((err, res) => {
            expect(res.status).to.be.eq(400);
            done();
        });
    });

    it("Should update the previously created comment", (done) => {
        mockComment.text = "What a comment?! (edited)";
        request.put("/comment/" + mockComment.id).send(mockComment).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });


    it("Should load the previously created comment", (done) => {
        request.get("/comment/" + mockComment.id).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });


    it("Should load the comments created by user", (done) => {
        request.get("/comment/user/" + mockComment.userId).end((err, res) => {
            expect(err).to.be.null;
            expect(res.body.length).to.be.gt(0);
            expect(res.status).to.be.eq(200);
            done();
        });
    });


    it("Should aggregate top mentions", (done) => {
        request.get("/comment/aggregate/top/mentions").end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            expect(res.body.length).to.be.gt(0);
            done();
        });
    });

    it("Should aggregate top mentions", (done) => {
        request.get("/comment/aggregate/top/hashtags").end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            expect(res.body.length).to.be.gt(0);
            done();
        });
    });

    it("Should delete the previously created comment", (done) => {
        request.delete("/comment/" + mockComment.id).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });

    it("Should fail loading a non existant comment", (done) => {
        request.get("/comment/" + mockComment.id).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(404);
            done();
        });
    });

});