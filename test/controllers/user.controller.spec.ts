import { expect } from "chai";
import { describe, it } from "mocha";
import * as supertest from "supertest";
import { IUser } from "../../src/models/user";
import { TestContext } from "../test.context";
import { mockUser } from "../test.data";

describe("User controller test", () => {

    const request = supertest.agent(TestContext.apiEndpoint);

    it("Should create a user successfully", (done) => {
        request.post("/user").send(mockUser).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });

    it("Should fail creating the same a user as before", (done) => {
        request.post("/user").send(mockUser).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(400);
            done();
        });
    });

    it("Should reject the invalid User creation request", (done) => {
        const invalidUser = {};
        request.post("/user").send(invalidUser).end((err, res) => {
            expect(res.status).to.be.eq(400);
            done();
        });
    });

    it("Should update the previously created user", (done) => {
        mockUser.contact.email = "name@mail.com";
        request.put("/user/" + mockUser.id).send(mockUser).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });


    it("Should load the previously created user", (done) => {
        request.get("/user/" + mockUser.id).end((err, res) => {
            expect(err).to.be.null;
            expect((res.body as IUser).contact.email).to.be.eq("name@mail.com")
            expect(res.status).to.be.eq(200);
            done();
        });
    });

    it("Should delete the previously created user", (done) => {
        request.delete("/user/" + mockUser.id).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(200);
            done();
        });
    });

    it("Should fail loading a non existant user", (done) => {
        request.get("/user/" + mockUser.id).end((err, res) => {
            expect(err).to.be.null;
            expect(res.status).to.be.eq(404);
            done();
        });
    });
});